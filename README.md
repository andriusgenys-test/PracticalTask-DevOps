# PracticalTask DevOps

Prerequisite you need to run this project locally:
* Node.js


## Run App locally
1. Clone repository to your location
2. In cloned location run `npm install express` to install all required packages
3. Type command `node app.js` to run cloned application
4. You should see "Example app listening on port 3000", this means that application is up and running now and you can access it by typing https://localhost:3000/ in your browser.

## List of tasks
- **Can listen for HTTP requests on a port of your choice**: Application listens to 3000 port
- **On startup, will HTTP POST the URL it's reachable at to a specified $ENDPOINT**: In browser use `/page` endpoint to see HTTP POST URL 
- **Will accept a JSON-formatted HTTP POST request, example: `{"hello":"some text value"}`**: Use `/health-check` endpoint to see JSON-formatted HTTP post request
- **Can print out in STDOUT or even display a HTML page with the received "some text value"**: After application is up and running, type https://localhost:3000/ and HTML page will be displayed


## CI/CD pipeline
In cloned repository you will find a `.gitlab-ci.yml`. Whole CI/CD pipeline have 5 stages - validate, plan, apply, publish, deploy.

### Create AWS EC2 instance
For IaC part I used Terraform, which files are stored in `/IaC` folder. First three **validate**, **plan**, **apply** stages are responsible for EC2 creation in AWS cloud.

### Package application
In order to deploy my application I used Docker to create an image with application files inside in **publish** stage. To do that I used docker in docker solution. Once image is created pipeline will store it in AWS ECR. 

### Uploads and runs your application on the created instance
