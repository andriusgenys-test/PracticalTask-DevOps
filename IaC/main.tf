resource "aws_instance" "OneServer" {
  ami           = var.ec2_image
  instance_type = var.ec2_instance_type

  tags = {
    Name = var.ec2_tags
  }
}