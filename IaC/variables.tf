variable "ec2_region" {
  default = "eu-central-1"
}

variable "ec2_image" {
  description = "Ubuntu AMI ID in eu-central-1 region"
  default = "ami-03aefa83246f44ef2"
}

variable "ec2_instance_type" {
  description = "Instance type"
  default = "t2.micro"
}

variable "ec2_tags" {
  description = "Name of the EC2 instance"
  default = "Demo EC2 instance"
}
