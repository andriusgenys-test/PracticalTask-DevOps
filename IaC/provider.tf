provider "aws" {
  version    = "~> 4.0"
  region     = var.ec2_region
}