const express = require('express')
const app = express()
const port = 3000
const path = require('path');
const router = express.Router();

const bodyParser = require('body-parser')

// Can print out in STDOUT or even display a HTML page with the received "some text value"
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname+'/www/index.html'))
});

// On startup, will HTTP POST the URL it's reachable at to a specified $ENDPOINT
app.post('/page', function (req, res) {    
  const protocol = req.protocol;
  const host = req.hostname;
  const url = req.originalUrl;

  const fullUrl = `${protocol}://${host}:${port}${url}`
    
  const responseString = `Your URL is: ${fullUrl}`;                       
  res.send(responseString);  
});

// Will accept a JSON-formatted HTTP POST request, example: `{"hello":"some text value"}`
app.post("/health-check", (req, res) => {
	res.json({ "hello": "Server up and running"  });
});


// Can listen for HTTP requests on a port of your choice
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
